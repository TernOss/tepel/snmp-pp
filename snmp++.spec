Name:          snmp++
Version:       3.5.2
Release:       1%{?dist}
Summary:       SNMP++ API
License:       Apache-2.0
Group:         Development/Libraries/C and C++

Url:           https://agentpp.com/api/cpp/snmp_pp.html
Source0:       https://agentpp.com/download/%{name}-%{version}.tar.gz
Patch0:        snmp-pp-cmakelists.patch

%if 0%{?suse_version}
BuildRequires: cmake gcc-c++ libopenssl-1_1-devel
%else
BuildRequires: cmake gcc-c++ openssl-devel
%endif

%description
SNMP++v3.x extends the original SNMP++v2.8 by the following:

SNMPv3 including User Security Model (USM) with:
MD5 and SHA authentication
DES, 3DES, AES and IDEA privacy
Thread-safety
Bug-fixes
These extension have been implemented transparently for the API user, except for the Pdu and Snmp classes which had been modified in order to support SNMPv3. Additionally, the API had been extended by a new Target subclass named UTarget (USM based Target).

SNMP++ v3.x (and AGENT++ v4.x) can be used with at least Linux, FreeBSD, Mac OS X, Solaris, Windows XP/7/8, and Cygwin.

%package devel
Summary: SNMP++ development files
Requires: %{name} = %{version}-%{release}

%description devel
This package contains necessary header files for SNMP++ development.

%prep
%setup
%patch0

%build
%cmake -DCMAKE_INSTALL_LIBDIR=%{_libdir}
%cmake_build

%install
%if 0%{?suse_version} >= 15
cd build
%endif
%cmake_install

%post
%{run_ldconfig}
%postun
%{run_ldconfig}

%files
%defattr(-,root,root,-)
%doc
%{_libdir}/libsnmp++.so*

%files devel
%defattr(-,root,root,-)
%doc
%dir %{_includedir}/snmp_pp
%{_includedir}/libsnmp.h
%{_includedir}/snmp_pp/*.h
%{_libdir}/*.a

%changelog
* Tue Nov 12 2024 Ari Guðfinnsson <ari@tern.is> 3.5.2-1
Update to version 3.5.2
* Thu Nov  4 2021 Ari Guðfinnsson <ari@tern.is> -added
Initial Commit 

