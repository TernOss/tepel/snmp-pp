# snmp++ RPM build spec

[![Copr build status](https://copr.fedorainfracloud.org/coprs/laridae/snmp_pp/package/snmp++/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/laridae/snmp_pp/package/snmp++/)

See further:
https://agentpp.com/api/cpp/snmp_pp.html
